import React, {useEffect} from 'react';
import {BrowserRouter, Link, Route, Routes, useParams} from "react-router-dom";
import ReactDOM from "react-dom";
import axios from "axios";
import * as cheerio from 'cheerio';
import {useAtom} from "jotai";
import {countURLChangeAtom, firstPageURLAtom} from "./atoms";

const Content = (props: {url: string | undefined, title: string, content: string}) => {
  console.log("Content di render", props.url);

  return (
    <>
      <h1>{props.title}</h1>
      <p>{props.content}</p>
    </>
  )
}


const Article = () => {

  const params = useParams();
  const [firstPageURL, ] = useAtom(firstPageURLAtom);
  const [countURLChange, setCountURLChange] = useAtom(countURLChangeAtom);
  console.log("countURLChange", countURLChange);

  useEffect(() => {
    console.log("change page");

    if(countURLChange > 0) {

      axios.get(`http://localhost:8080/wiki/${params.article_name}`)
        .then((response) => {
          if(response.status === 200) {
            const html = response.data;

            const $ = cheerio.load(html);
            console.log($.html());
            const wikiURL = $('meta[name="wiki-url"]').attr('content');
            console.log("ini wikiURL nya", wikiURL);

            // If article_name !== page
            console.log(`/wiki/${params.article_name}`, wikiURL);
            if(firstPageURL !== wikiURL) {
              console.log("INDONESIA JUARAAA");
              ReactDOM.render(
                <React.StrictMode>
                  <BrowserRouter>
                    <Content
                      url={params.article_name}
                      content={$('#content p').text()}
                      title={$('#content h1').text()}
                    />
                  </BrowserRouter>
                </React.StrictMode>,
                document.getElementById('content')
              );
            }
          }
        }, err => console.log(err))
    }

    setCountURLChange(countURLChange + 1);

  }, [params.article_name]);

  return (
    <div>
      <nav>
        <Link to={`/`}>Home</Link>
        <Link to={`/wiki/article-1`}>Article 1</Link>
        <Link to={`/wiki/article-2`}>Article 2</Link>
        <Link to={`/wiki/article-3`}>Article 3</Link>
        <Link to={`/wiki/article-4`}>Article 4</Link>
      </nav>
    </div>
  )
}

const Home = () => {
  const [countURLChange, setCountURLChange] = useAtom(countURLChangeAtom);
  const [firstPageURL, setFirstPageURL] = useAtom(firstPageURLAtom);

  useEffect(() => {
    setCountURLChange(countURLChange + 1);
  }, []);
  return (
    <div>
      <main>
        This is home
      </main>
      <nav>
        <Link to={`/wiki/article-1`}>Article 1</Link>
        <Link to={`/wiki/article-2`}>Article 2</Link>
        <Link to={`/wiki/article-3`}>Article 3</Link>
        <Link to={`/wiki/article-4`}>Article 4</Link>
      </nav>
    </div>
  )
}

const App = () => {
  return (
    <div className="App">
      <Routes>
        <Route path={`/`} element={<Home/>}/>
        <Route path={`/wiki/:article_name`} element={<Article/>}/>
      </Routes>
    </div>
  )
};

export default App;
