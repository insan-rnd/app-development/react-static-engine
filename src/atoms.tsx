import {atom} from "jotai";

export const firstPageURLAtom = atom('');
export const countURLChangeAtom = atom(0);
